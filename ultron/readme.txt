#https://docs.python.org/3/library/venv.html#creating-virtual-environments
#Create the virtual environment
python3 -m venv ./venv

#Activate it
source venv/bin/activate

#Install dependencies
pip3 install -e .


#Do test run
python ultron

#Deactivate venv
deactivate


#Additional documentation
https://github.com/shibdib/Firetail/wiki/1.-Install



